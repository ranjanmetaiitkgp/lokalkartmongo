package com.lokalkart.service;

import java.util.List;

import com.lokalkart.domain.Country;

public interface CountryService {

	List<Country> getAllCountry();
	Country saveCountry(Country country);
	Country getCountryById(String countryId);
}
