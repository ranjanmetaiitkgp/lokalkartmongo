package com.lokalkart.service;

import com.lokalkart.domain.AppImage;

public interface AppImageService {

	AppImage saveAppImage(AppImage appImage);
}
