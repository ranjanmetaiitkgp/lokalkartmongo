package com.lokalkart.service;

public interface UserSessionService{
	
	boolean invalidateSession(String sessionId);

}
