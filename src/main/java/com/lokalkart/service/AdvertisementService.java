package com.lokalkart.service;

import java.util.List;

import com.lokalkart.domain.Advertisement;
import com.lokalkart.domain.ProductSubCategory;
import com.lokalkart.domain.Retailer;


public interface AdvertisementService{
	
	List<Advertisement> getAdsByRetailer(Retailer retailer);
	List<Advertisement> getAdsByLocation(Long pinCode);
	List<Advertisement> getAdsByProductSubCategory(ProductSubCategory productSubCategory);

}
