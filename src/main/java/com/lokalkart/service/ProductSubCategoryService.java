package com.lokalkart.service;

import com.lokalkart.domain.ProductSubCategory;

public interface ProductSubCategoryService{

	ProductSubCategory saveProductSubCategory(ProductSubCategory productSubCategory);
	ProductSubCategory findBySubCatId(String subCatId);
}
