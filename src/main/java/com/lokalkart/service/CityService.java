package com.lokalkart.service;

import java.util.List;

import com.lokalkart.domain.City;

public interface CityService {

	List<City> getAllCityByState(String stateId);
	List<City> getAllCity();
	City saveCity(City city);
	City getCityById(String cityId);
}
