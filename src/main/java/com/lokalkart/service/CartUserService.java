package com.lokalkart.service;

import com.lokalkart.domain.CartUser;

public interface CartUserService{
	
	boolean signUpUser(CartUser user);
	boolean authenticateUser(CartUser user);
	boolean validateUser(CartUser user);

}
