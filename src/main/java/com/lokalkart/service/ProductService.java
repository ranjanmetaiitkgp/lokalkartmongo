package com.lokalkart.service;

import java.util.List;

import com.lokalkart.domain.Product;
import com.lokalkart.domain.Retailer;

public interface ProductService{
	
	List<Product> getProductListByRetailer(Retailer retailer);
	Product saveProduct(Product product);

}
