package com.lokalkart.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.lokalkart.domain.State;

public interface StateRepository extends MongoRepository<State, String>{
	
	List<State> findAllStateByCountry(String countryId);

}
