package com.lokalkart.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.lokalkart.domain.CartUser;

public interface CartUserRepository extends MongoRepository<CartUser, String>{

}
