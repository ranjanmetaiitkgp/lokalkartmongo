package com.lokalkart.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.lokalkart.domain.City;

public interface CityRepository extends MongoRepository<City, String>{

}
