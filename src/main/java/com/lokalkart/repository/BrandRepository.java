package com.lokalkart.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.lokalkart.domain.Brand;

public interface BrandRepository extends MongoRepository<Brand, String>{

	
}
