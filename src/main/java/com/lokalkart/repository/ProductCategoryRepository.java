package com.lokalkart.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.lokalkart.domain.ProductCategory;

public interface ProductCategoryRepository extends MongoRepository<ProductCategory, String>{

}
