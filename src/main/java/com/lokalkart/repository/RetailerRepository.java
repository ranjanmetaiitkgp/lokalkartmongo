package com.lokalkart.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.lokalkart.domain.Retailer;

public interface RetailerRepository extends MongoRepository<Retailer, String>{

	
}
