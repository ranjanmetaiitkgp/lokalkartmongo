package com.lokalkart.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.lokalkart.domain.AppImage;

public interface AppImageRepository extends MongoRepository<AppImage, String>{

}
