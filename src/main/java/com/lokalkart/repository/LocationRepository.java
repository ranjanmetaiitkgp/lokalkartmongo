package com.lokalkart.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.lokalkart.domain.Location;

public interface LocationRepository extends MongoRepository<Location, String>{

	List<Location> findAllLocationByCity(String cityId);
}
