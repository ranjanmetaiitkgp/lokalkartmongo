package com.lokalkart.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.lokalkart.domain.GuidedTour;

public interface GuidedTourRepository extends MongoRepository<GuidedTour, String>{

}
