package com.lokalkart.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.lokalkart.domain.Product;

public interface ProductRepository extends MongoRepository<Product, String>{

}
