package com.lokalkart.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.lokalkart.domain.ProductSubCategory;

public interface ProductSubCategoryRepository extends MongoRepository<ProductSubCategory, String>{
 
	ProductSubCategory findBySubCatId(String subCatId);
}
