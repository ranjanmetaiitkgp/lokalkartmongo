package com.lokalkart;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class CustomerController {
	
	@Autowired
	private CustomerRepository repository;

	//@RequestMapping(value= "/saveCustomer/{firstName}/{lastName}", method=RequestMethod.POST, produces={"application/json","application/xml"})
	//public @ResponseBody Customer saveCustomer(@PathVariable("firstName") String firstName, @PathVariable("lastName") String lastName) throws Exception{
	@RequestMapping(value= "/saveCustomer", method=RequestMethod.POST, produces={"application/json","application/xml"})
	public @ResponseBody Customer saveCustomer(@RequestBody Customer customer) throws Exception{
		Customer customerResp = run(customer.getFirstName(), customer.getLastName());
		return customerResp;
		//return run(firstName, lastName);
	}

	//@Override
	public Customer run(String firstName, String lastName) throws Exception {
		//this.repository.deleteAll();

		// save a couple of customers
		this.repository.save(new Customer(firstName, lastName));
		//this.repository.save(new Customer("Rahul", "Ranjan"));
		//this.repository.save(new Customer("Shaurya", "Krit"));
		//this.repository.save(new Customer("Khushboo", "Ranjan"));

		// fetch all customers
		System.out.println("Customers found with findAll():");
		System.out.println("-------------------------------");
		for (Customer customer : this.repository.findAll()) {
			System.out.println(customer);
		}
		System.out.println();
		
		return this.repository.findByFirstName(firstName);

		// fetch an individual customer
		/*System.out.println("Customer found with findByFirstName('Rohit'):");
		System.out.println("--------------------------------");
		System.out.println(this.repository.findByFirstName("Rohit"));

		System.out.println("Customers found with findByLastName('Shaurya'):");
		System.out.println("--------------------------------");
		for (Customer customer : this.repository.findByLastName("Shaurya")) {
			System.out.println(customer);
		}*/
	}
	
	
}
