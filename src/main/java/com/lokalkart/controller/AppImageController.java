package com.lokalkart.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.lokalkart.domain.AppImage;
import com.lokalkart.service.AppImageService;

@RestController
@RequestMapping("/appImage")
public class AppImageController {
	
	@Autowired
	private AppImageService appImageService;

	@RequestMapping(value = "/saveAppImage", method = RequestMethod.POST, produces = {"application/json","application/xml"})
	public AppImage saveAppImage(@RequestBody AppImage appImage){
		return appImageService.saveAppImage(appImage);
	}
}
