package com.lokalkart.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.lokalkart.domain.Advertisement;
import com.lokalkart.service.AdvertisementService;

@RestController("/advertisement")
public class AdvertisementController {
	
	@Autowired
	private AdvertisementService advertisementService;
	
	@RequestMapping(value = "/saveAdvertisement", method = RequestMethod.POST, produces = {"application/json","application/xml"})
	public Advertisement saveAdvertisement(@RequestBody Advertisement advertisement) {
		//advertisementService.save(advertisement);
		return null;
	}
}
