package com.lokalkart.controller;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.lokalkart.domain.AppImage;
import com.lokalkart.domain.Brand;
import com.lokalkart.domain.Product;
import com.lokalkart.domain.jsonelement.AppImageJSON;
import com.lokalkart.domain.jsonelement.BrandJSON;
import com.lokalkart.domain.jsonelement.ProductJSON;
import com.lokalkart.service.AppImageService;
import com.lokalkart.service.BrandService;

@RestController
@RequestMapping("/brand")
public class BrandController {

	@Autowired
	private BrandService brandService;
	
	@Autowired
	private AppImageService appImageService;

	@RequestMapping(value = "/saveBrand",  method = RequestMethod.POST, produces = {"application/json","application/xml"})
	public Brand saveBrand(@RequestBody BrandJSON brandJSON){
		Brand brand = new Brand();
		brand.setBrandName(brandJSON.getBrandName());
		Set<ProductJSON> productJSONs = brandJSON.getProducts();
		Set<Product> products = new LinkedHashSet<Product>();
		Iterator<ProductJSON> iterator = productJSONs.iterator();
		while(iterator.hasNext()){
			ProductJSON productJSON = iterator.next();
			Product product = new Product();
			product.setProductId(productJSON.getProductId());
			product.setProductName(productJSON.getProductName());
			products.add(product);
		}
		brand.setProducts(products);
		
		List<AppImageJSON> appImageJsonList = brandJSON.getProductImages();
		List<AppImage> appImageList = new ArrayList<>();
		if(appImageJsonList != null && !appImageJsonList.isEmpty()){
			for(AppImageJSON appImageJson : appImageJsonList){
				AppImage appImage = new AppImage();
				appImage.setImageSize(appImageJson.getImageSize());
				appImage.setImageUrl(appImageJson.getImageUrl());
				appImage = appImageService.saveAppImage(appImage);
				appImageList.add(appImage);
			}
		}
		brand.setProductImages(appImageList);
		brand = brandService.saveBrand(brand);
		return brand;
	}
}
