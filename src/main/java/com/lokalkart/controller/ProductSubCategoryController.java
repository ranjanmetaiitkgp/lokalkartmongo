package com.lokalkart.controller;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.lokalkart.domain.AppImage;
import com.lokalkart.domain.Brand;
import com.lokalkart.domain.Product;
import com.lokalkart.domain.ProductSubCategory;
import com.lokalkart.domain.jsonelement.AppImageJSON;
import com.lokalkart.domain.jsonelement.BrandJSON;
import com.lokalkart.domain.jsonelement.ProductJSON;
import com.lokalkart.domain.jsonelement.ProductSubCategoryJSON;
import com.lokalkart.service.AppImageService;
import com.lokalkart.service.ProductSubCategoryService;

//@RestController

@RestController
@RequestMapping("/productSubCategory")
public class ProductSubCategoryController {
	
	@Autowired
	private ProductSubCategoryService productSubCategoryService;
	
	@Autowired
	private AppImageService appImageService;

	@RequestMapping(value = "/saveProductSubCategory",  method = RequestMethod.POST, produces = {"application/json","application/xml"})
	public ProductSubCategory saveProductSubCategory(@RequestBody ProductSubCategoryJSON productSubCategoryJSON){
		ProductSubCategory productSubCategory = new ProductSubCategory();
		productSubCategory.setSubCategoryName(productSubCategoryJSON.getSubCategoryName());
		
		List<AppImageJSON> imageJsonList = productSubCategoryJSON.getProductSubCategoryImage();
		List<AppImage> images = new ArrayList<>();
		if(imageJsonList !=null && !imageJsonList.isEmpty()){
			for(AppImageJSON appImageJSON : imageJsonList){
				AppImage image = new AppImage();
				image.setImageUrl(appImageJSON.getImageUrl());
				image.setImageSize(appImageJSON.getImageSize());
				image = appImageService.saveAppImage(image);
				images.add(image);
			}
		}
		
		productSubCategory.setProductSubCategoryImage(images);
		
		Set<Brand> brands = new LinkedHashSet<Brand>();
		Set<Product> prods = null;
		Iterator<BrandJSON> brandIt = productSubCategoryJSON.getBrand().iterator();
		while(brandIt.hasNext()){
			BrandJSON brandJSON = (BrandJSON) brandIt.next();
			Brand brand = new Brand();
			brand.setBrandId(brandJSON.getBrandId());
			brand.setBrandName(brandJSON.getBrandName());
			prods = new LinkedHashSet<Product>();
			Iterator<ProductJSON> prodIt = brandJSON.getProducts().iterator();
			while(prodIt.hasNext()){
				ProductJSON productJSON = (ProductJSON) prodIt.next();
				Product product = new Product();
				product.setProductId(productJSON.getProductId());
				product.setProductName(productJSON.getProductName());
				prods.add(product);
			}
			brand.setProducts(prods);
			brands.add(brand);
		}
		productSubCategory.setBrand(brands);
		productSubCategory =  productSubCategoryService.saveProductSubCategory(productSubCategory);
		return productSubCategory;
	}
}
