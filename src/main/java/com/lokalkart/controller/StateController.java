package com.lokalkart.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.lokalkart.domain.Country;
import com.lokalkart.domain.State;
import com.lokalkart.domain.jsonelement.StateJSON;
import com.lokalkart.service.CountryService;
import com.lokalkart.service.StateService;

@RestController
@RequestMapping("/state")
public class StateController {
	
	@Autowired
	private StateService stateService;
	
	@Autowired
	private CountryService countryService;
	
	@RequestMapping(value = "/saveState",  method = RequestMethod.POST, produces = {"application/json","application/xml"})
	public State saveState(@RequestBody StateJSON stateJson){
		State state = new State();
		state.setStateName(stateJson.getStateName());
		state.setStateCode(stateJson.getStateCode());
		Country country = countryService.getCountryById(stateJson.getCountryId());
		state.setCountry(country);
		return stateService.saveState(state);
	}

}
