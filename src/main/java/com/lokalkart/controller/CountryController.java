package com.lokalkart.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.lokalkart.domain.Country;
import com.lokalkart.domain.jsonelement.CountryJSON;
import com.lokalkart.service.CountryService;

@RestController

@RequestMapping("/country")
public class CountryController {
	
	@Autowired
	private CountryService countryService;
	
	@RequestMapping(value = "/saveCountry",  method = RequestMethod.POST, produces = {"application/json","application/xml"})
	public Country saveCountry(@RequestBody CountryJSON countryJson){
		Country country = new Country();
		country.setCountryName(countryJson.getCountryName());
		country.setCountryCode(countryJson.getCountryCode()); 
		return countryService.saveCountry(country);
	}

}
