package com.lokalkart.serviceImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.lokalkart.domain.City;
import com.lokalkart.repository.CityRepository;
import com.lokalkart.service.CityService;

@Service
public class CityServiceImpl implements CityService{
	
	@Autowired
	private CityRepository cityRepository;

	@Override
	public List<City> getAllCityByState(String stateId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<City> getAllCity() {
		return cityRepository.findAll();
	}

	@Override
	public City saveCity(City city) {
		return cityRepository.save(city);
	}

	@Override
	public City getCityById(String cityId) {
		return cityRepository.findOne(cityId);
	}

}
