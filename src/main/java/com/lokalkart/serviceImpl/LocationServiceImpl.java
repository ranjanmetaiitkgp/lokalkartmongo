package com.lokalkart.serviceImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.lokalkart.domain.Location;
import com.lokalkart.repository.LocationRepository;
import com.lokalkart.service.LocationService;

@Service
public class LocationServiceImpl implements LocationService{

	@Autowired
	private LocationRepository locationRepository;
	
	@Override
	public List<Location> getAllLocationByCity(String cityId) {
		return locationRepository.findAllLocationByCity(cityId);
	}

	@Override
	public Location saveLocation(Location location) {
		return locationRepository.save(location);
	}

}
