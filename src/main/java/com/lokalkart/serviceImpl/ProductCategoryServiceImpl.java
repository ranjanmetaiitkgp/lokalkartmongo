package com.lokalkart.serviceImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.lokalkart.domain.ProductCategory;
import com.lokalkart.repository.ProductCategoryRepository;
import com.lokalkart.service.ProductCategoryService;

@Service
public class ProductCategoryServiceImpl implements ProductCategoryService{

	@Autowired
	private ProductCategoryRepository productCategoryRepository;
	
	@Override
	public ProductCategory saveProductCategory(ProductCategory productCategory) {
		return productCategoryRepository.save(productCategory);
	}

	@Override
	public List<ProductCategory> getAllProductCategory() {
		return productCategoryRepository.findAll();
	}

	@Override
	public ProductCategory getProductCategory(String catId) {
		return productCategoryRepository.findOne(catId);
	}

	@Override
	public ProductCategory updateProductCategory(ProductCategory productCategory) {
		return productCategoryRepository.save(productCategory);
	}



}
