package com.lokalkart.serviceImpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.lokalkart.domain.Brand;
import com.lokalkart.repository.BrandRepository;
import com.lokalkart.service.BrandService;

@Service
public class BrandServiceImpl implements BrandService{

	@Autowired
	private BrandRepository brandRepository;
	
	@Override
	public Brand saveBrand(Brand brand) {
		return brandRepository.save(brand);
	}

}
