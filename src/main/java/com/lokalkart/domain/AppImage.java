package com.lokalkart.domain;

import java.io.Serializable;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * 
 * @author Rohit Ranjan
 *
 */
@Document
public class AppImage implements Serializable{

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 7743812216448320171L;

	@Id
	private String id;
	
	private String imageUrl;
	
	private ProductImageSize imageSize;
	
	public String getImageUrl() {
		return imageUrl;
	}
	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}
	public ProductImageSize getImageSize() {
		return imageSize;
	}
	public void setImageSize(ProductImageSize imageSize) {
		this.imageSize = imageSize;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}

}
