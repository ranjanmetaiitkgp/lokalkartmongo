package com.lokalkart.domain;

import java.io.Serializable;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * 
 * @author Rohit Ranjan
 *
 */
@Document
public class Advertisement implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -8718540510005382120L;
	
	@Id
	private String advertisementId;
	
	private Retailer retailer;
	private AppImage adImage;
	private Product product;
	
	public Retailer getRetailer() {
		return retailer;
	}
	public void setRetailer(Retailer retailer) {
		this.retailer = retailer;
	}
	public AppImage getAdImage() {
		return adImage;
	}
	public void setAdImage(AppImage adImage) {
		this.adImage = adImage;
	}
	public Product getProduct() {
		return product;
	}
	public void setProduct(Product product) {
		this.product = product;
	}
	public String getAdvertisementId() {
		return advertisementId;
	}
	public void setAdvertisementId(String advertisementId) {
		this.advertisementId = advertisementId;
	}

}
