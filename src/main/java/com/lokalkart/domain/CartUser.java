package com.lokalkart.domain;

import java.io.Serializable;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * 
 * @author Rohit Ranjan
 *
 */
@Document
public class CartUser implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 2903059180690699226L;
	
	@Id
	private String userId;
	
	@Indexed(unique=true)
	private String userName;
	
	private String token;
	
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getToken() {
		return token;
	}
	public void setToken(String token) {
		this.token = token;
	}

}
