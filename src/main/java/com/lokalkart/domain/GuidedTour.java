package com.lokalkart.domain;

import java.io.Serializable;
import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * 
 * @author Rohit Ranjan
 *
 */

@Document
public class GuidedTour implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -2311151026574181780L;

	@Id
	private String tourId;
	
	private ProductSubCategory subCat;
	
	private List<TourSet> tourSets;
	
	private class TourSet{
		
		private AppImage image;
		private String description;
		
	}

	public String getTourId() {
		return tourId;
	}

	public void setTourId(String tourId) {
		this.tourId = tourId;
	}

	public ProductSubCategory getSubCat() {
		return subCat;
	}

	public void setSubCat(ProductSubCategory subCat) {
		this.subCat = subCat;
	}

	public List<TourSet> getTourSets() {
		return tourSets;
	}

	public void setTourSets(List<TourSet> tourSets) {
		this.tourSets = tourSets;
	}
}
