package com.lokalkart.domain;

import java.io.Serializable;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class Brand implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -468110846359631450L;
	
	@Id
	private String brandId;
	
	public String getBrandId() {
		return brandId;
	}

	public void setBrandId(String brandId) {
		this.brandId = brandId;
	}

	public String getBrandName() {
		return brandName;
	}

	public void setBrandName(String brandName) {
		this.brandName = brandName;
	}

	@Indexed
	private String brandName;
	
	@DBRef
	private Set<Product> products;
	
	@DBRef
	private List<AppImage> productImages;

	public List<AppImage> getProductImages() {
		return productImages;
	}

	public void setProductImages(List<AppImage> productImages) {
		this.productImages = productImages;
	}

	public Set<Product> getProducts() {
		if(null == products){
			products = new LinkedHashSet<Product>();
		}
		return products;
	}

	public void setProducts(Set<Product> products) {
		this.products = products;
	}
}
