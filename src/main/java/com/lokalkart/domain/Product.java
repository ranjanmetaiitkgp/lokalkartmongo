package com.lokalkart.domain;

import java.io.Serializable;
import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import com.lokalkart.interfaces.TechnicalSpecification;


/**
 * 
 * @author Rohit Ranjan
 *
 */

@Document
public class Product implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -1721987419101037995L;
	
	@Id
	private String productId;
	
	@Indexed
	private String itemCode;
	
	@Indexed
	private String productName;
	
	private String productDescription;
	
	//@TODO change double type to BigInteger
	private Double maxRetailPrice;
	private Double discountPercentage;
	private Double sellingPrice;
	
	@DBRef
	private List<AppImage> productImages;
	
	private List<PaymentType> paymentTypes;
	
	private String deliveryTime;
	
	private Double deliveryCharge;
	
	private TechnicalSpecification techSpecification;
	
	private List<String> keyFeatures;
	
	private String productSubCategoryId;
	
	public String getProductSubCategoryId() {
		return productSubCategoryId;
	}

	public void setProductSubCategoryId(String productSubCategoryId) {
		this.productSubCategoryId = productSubCategoryId;
	}

	@DBRef
	private Retailer retailer;

	public String getProductId() {
		return productId;
	}

	public void setProductId(String productId) {
		this.productId = productId;
	}

	public String getItemCode() {
		return itemCode;
	}

	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getProductDescription() {
		return productDescription;
	}

	public void setProductDescription(String productDescription) {
		this.productDescription = productDescription;
	}

	public Double getMaxRetailPrice() {
		return maxRetailPrice;
	}

	public void setMaxRetailPrice(Double maxRetailPrice) {
		this.maxRetailPrice = maxRetailPrice;
	}

	public Double getDiscountPercentage() {
		return discountPercentage;
	}

	public void setDiscountPercentage(Double discountPercentage) {
		this.discountPercentage = discountPercentage;
	}

	public Double getSellingPrice() {
		return sellingPrice;
	}

	public void setSellingPrice(Double sellingPrice) {
		this.sellingPrice = sellingPrice;
	}

	public List<AppImage> getProductImages() {
		return productImages;
	}

	public void setProductImages(List<AppImage> productImages) {
		this.productImages = productImages;
	}

	public List<PaymentType> getPaymentTypes() {
		return paymentTypes;
	}

	public void setPaymentTypes(List<PaymentType> paymentTypes) {
		this.paymentTypes = paymentTypes;
	}

	public String getDeliveryTime() {
		return deliveryTime;
	}

	public void setDeliveryTime(String deliveryTime) {
		this.deliveryTime = deliveryTime;
	}

	public Double getDeliveryCharge() {
		return deliveryCharge;
	}

	public void setDeliveryCharge(Double deliveryCharge) {
		this.deliveryCharge = deliveryCharge;
	}

	public TechnicalSpecification getTechSpecification() {
		return techSpecification;
	}

	public void setTechSpecification(TechnicalSpecification techSpecification) {
		this.techSpecification = techSpecification;
	}

	public List<String> getKeyFeatures() {
		return keyFeatures;
	}

	public void setKeyFeatures(List<String> keyFeatures) {
		this.keyFeatures = keyFeatures;
	}

	public Retailer getRetailer() {
		return retailer;
	}

	public void setRetailer(Retailer retailer) {
		this.retailer = retailer;
	}
	//@TODO Product Review and Ratings

}
