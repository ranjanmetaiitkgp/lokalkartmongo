package com.lokalkart.domain;

import java.io.Serializable;
import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * 
 * @author Rohit Ranjan
 *
 */
@Document
public class Retailer implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 5133650324647129283L;

	@Id
	private String retailerId;
	
	@Indexed
	private String retailerName;
	
	@DBRef
	private List<Address> address;

	public String getRetailerId() {
		return retailerId;
	}

	public void setRetailerId(String retailerId) {
		this.retailerId = retailerId;
	}

	public String getRetailerName() {
		return retailerName;
	}

	public void setRetailerName(String retailerName) {
		this.retailerName = retailerName;
	}

	public List<Address> getAddress() {
		return address;
	}

	public void setAddress(List<Address> address) {
		this.address = address;
	}

}
