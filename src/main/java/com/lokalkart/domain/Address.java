package com.lokalkart.domain;

import java.io.Serializable;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class Address implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -2798549886863535433L;
	
	@Id
	private String addressId;
	
	private String firstLine;
	
	private String secondLine;
	
	@DBRef
	private Location location;
	
	@DBRef
	private City city;
	
	@DBRef
	private State state;
	
	@DBRef
	private Country country;
	
	@Indexed
	private Long pinCode;
	
	private String contactNumber;
	
	public String getFirstLine() {
		return firstLine;
	}
	public void setFirstLine(String firstLine) {
		this.firstLine = firstLine;
	}
	public String getSecondLine() {
		return secondLine;
	}
	public void setSecondLine(String secondLine) {
		this.secondLine = secondLine;
	}
	
	public Long getPinCode() {
		return pinCode;
	}
	public void setPinCode(Long pinCode) {
		this.pinCode = pinCode;
	}
	public String getContactNumber() {
		return contactNumber;
	}
	public void setContactNumber(String contactNumber) {
		this.contactNumber = contactNumber;
	}
	public String getAddressId() {
		return addressId;
	}
	public void setAddressId(String addressId) {
		this.addressId = addressId;
	}
	public Location getLocation() {
		return location;
	}
	public void setLocation(Location location) {
		this.location = location;
	}
	public void setCity(City city) {
		this.city = city;
	}
	public void setState(State state) {
		this.state = state;
	}
	public void setCountry(Country country) {
		this.country = country;
	}
	public City getCity() {
		return city;
	}
	public State getState() {
		return state;
	}
	public Country getCountry() {
		return country;
	}
}
