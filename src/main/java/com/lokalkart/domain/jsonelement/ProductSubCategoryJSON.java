package com.lokalkart.domain.jsonelement;

import java.util.List;
import java.util.Set;

public class ProductSubCategoryJSON {
	
	private String subCatId;
	private String subCategoryName;
	private List<AppImageJSON> productSubCategoryImage;
	private Set<BrandJSON> brand;
	
	
	public Set<BrandJSON> getBrand() {
		return brand;
	}
	public void setBrand(Set<BrandJSON> brand) {
		this.brand = brand;
	}
	public String getSubCatId() {
		return subCatId;
	}
	public void setSubCatId(String subCatId) {
		this.subCatId = subCatId;
	}
	public String getSubCategoryName() {
		return subCategoryName;
	}
	public void setSubCategoryName(String subCategoryName) {
		this.subCategoryName = subCategoryName;
	}
	public List<AppImageJSON> getProductSubCategoryImage() {
		return productSubCategoryImage;
	}
	public void setProductSubCategoryImage(List<AppImageJSON> productSubCategoryImage) {
		this.productSubCategoryImage = productSubCategoryImage;
	}

}
