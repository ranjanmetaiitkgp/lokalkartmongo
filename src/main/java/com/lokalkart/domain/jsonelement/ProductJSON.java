package com.lokalkart.domain.jsonelement;

import java.io.Serializable;
import java.util.List;

import com.lokalkart.domain.PaymentType;
import com.lokalkart.interfaces.TechnicalSpecification;

public class ProductJSON implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -2962491710590056279L;
	
	private String productId;
	private String itemCode;
	private String productName;
	private String productDescription;
	
	//@TODO change double type to BigInteger
	private Double maxRetailPrice;
	
	private Double discountPercentage;
	private Double sellingPrice;
	private List<AppImageJSON> productImages;
	private List<PaymentType> paymentTypes;
	private String productSubCategoryId;
	private String deliveryTime;
	private Double deliveryCharge;
	private TechnicalSpecification techSpecification;
	private List<String> keyFeatures;
	private String retailerId;
	public String getProductId() {
		return productId;
	}
	public void setProductId(String productId) {
		this.productId = productId;
	}
	public String getItemCode() {
		return itemCode;
	}
	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	public String getProductDescription() {
		return productDescription;
	}
	public void setProductDescription(String productDescription) {
		this.productDescription = productDescription;
	}
	public Double getMaxRetailPrice() {
		return maxRetailPrice;
	}
	public void setMaxRetailPrice(Double maxRetailPrice) {
		this.maxRetailPrice = maxRetailPrice;
	}
	public Double getDiscountPercentage() {
		return discountPercentage;
	}
	public void setDiscountPercentage(Double discountPercentage) {
		this.discountPercentage = discountPercentage;
	}
	public Double getSellingPrice() {
		return sellingPrice;
	}
	public void setSellingPrice(Double sellingPrice) {
		this.sellingPrice = sellingPrice;
	}
	public List<AppImageJSON> getProductImages() {
		return productImages;
	}
	public void setProductImages(List<AppImageJSON> productImages) {
		this.productImages = productImages;
	}
	public List<PaymentType> getPaymentTypes() {
		return paymentTypes;
	}
	public void setPaymentTypes(List<PaymentType> paymentTypes) {
		this.paymentTypes = paymentTypes;
	}
	public String getProductSubCategoryId() {
		return productSubCategoryId;
	}
	public void setProductSubCategoryId(String productSubCategoryId) {
		this.productSubCategoryId = productSubCategoryId;
	}
	public String getDeliveryTime() {
		return deliveryTime;
	}
	public void setDeliveryTime(String deliveryTime) {
		this.deliveryTime = deliveryTime;
	}
	public Double getDeliveryCharge() {
		return deliveryCharge;
	}
	public void setDeliveryCharge(Double deliveryCharge) {
		this.deliveryCharge = deliveryCharge;
	}
	public TechnicalSpecification getTechSpecification() {
		return techSpecification;
	}
	public void setTechSpecification(TechnicalSpecification techSpecification) {
		this.techSpecification = techSpecification;
	}
	public List<String> getKeyFeatures() {
		return keyFeatures;
	}
	public void setKeyFeatures(List<String> keyFeatures) {
		this.keyFeatures = keyFeatures;
	}
	public String getRetailerId() {
		return retailerId;
	}
	public void setRetailerId(String retailerId) {
		this.retailerId = retailerId;
	} 
}
