package com.lokalkart.domain.jsonelement;

import java.io.Serializable;

public class StateJSON implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 3028472547687927279L;

	private String stateId;
	
	private String stateName;
	
	private String stateCode;
	
	private String countryId;

	public String getStateId() {
		return stateId;
	}

	public void setStateId(String stateId) {
		this.stateId = stateId;
	}

	public String getStateName() {
		return stateName;
	}

	public void setStateName(String stateName) {
		this.stateName = stateName;
	}

	public String getStateCode() {
		return stateCode;
	}

	public void setStateCode(String stateCode) {
		this.stateCode = stateCode;
	}

	public String getCountryId() {
		return countryId;
	}

	public void setCountryId(String countryId) {
		this.countryId = countryId;
	}

}
