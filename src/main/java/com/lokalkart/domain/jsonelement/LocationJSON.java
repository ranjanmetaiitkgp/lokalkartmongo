package com.lokalkart.domain.jsonelement;

import java.io.Serializable;

public class LocationJSON implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -4916989733863822500L;

	private String locationId;
	
	private String locationName;
	
	private String cityId;

	public String getLocationId() {
		return locationId;
	}

	public void setLocationId(String locationId) {
		this.locationId = locationId;
	}

	public String getLocationName() {
		return locationName;
	}

	public void setLocationName(String locationName) {
		this.locationName = locationName;
	}

	public String getCityId() {
		return cityId;
	}

	public void setCityId(String cityId) {
		this.cityId = cityId;
	}
}
